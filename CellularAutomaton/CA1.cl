
#define     BLOCK_SIZE      128

__constant
const  int  RULE[8] = {
    //  000 001 010 011 100 101 110 111
    /**/ 0,  1,  0,  1,  1,  0,  1,  0
};

__kernel  void
runCA90(
        __global  const  int  *     input1,
        __global  int  *            output,
        __local   int  *            work)
{
    int  index  = get_global_id(0) + 1;
    int  lx     = get_local_id(0)  + 1;

    work[lx]    = input1[index];
    if ( lx == 1 ) {
        work[0]             = input1[index - 1];
        work[BLOCK_SIZE +1] = input1[index + BLOCK_SIZE];
    }
    barrier(CLK_LOCAL_MEM_FENCE);

    int  before = (work[lx - 1] * 4) + (work[lx] * 2) + (work[lx +1]);
    output[index]   = RULE[before];
////    output[index]   = (work[lx - 1] ^ work[lx + 1]);
}
