﻿//  -*-  mode: c++; coding: utf-8  -*-  //
/*************************************************************************
**                                                                      **
**                      ---   OpenCL Wrapper   ---                      **
**                                                                      **
**      Copyright (c), Takahiro Itou, 2013-2014.                        **
**      All Rights Reserved.                                            **
**                                                                      **
*************************************************************************/

/**
**      An Interface of OclWrap class.
**
**      @file       Common/OclWrap.h
**/

#if !defined( OCLPROJ_COMMON_INCLUDED_OCL_WRAP_H )
#    define   OCLPROJ_COMMON_INCLUDED_OCL_WRAP_H

#include    <iosfwd>
#include    <string>
#include    <vector>
#include    <CL/cl.hpp>

//  クラスの前方宣言。  //

namespace  CLWRAP  {

//========================================================================
//
//    ErrCode.
//
/**
**    エラーコード型。
**/
typedef  enum  {
    ERR_SUCCESS = 0,            /**<  正常終了。    **/
    ERR_FAILURE = 1,            /**<  異常終了。    **/

    ERR_FILE_NOT_FOUND  = 2,    /**<  ファイルが見つからない。  **/
}  ErrCode;

//========================================================================
//
//    OclWrap  class.
//
/**
**    ラッパークラス。
**/

class  OclWrap
{

//========================================================================
//
//    初期化と終了。
//
public:

    //----------------------------------------------------------------
    /**   インスタンスを初期化する。
    **
    **  @param[out] os    ログ出力用ストリーム。
    **  @return     エラーコードを返す。
    **      -   異常終了時はエラーの種類を示す非ゼロ値を返す。
    **      -   正常終了の場合はゼロを返す。
    **/
    const   ErrCode
    setupInstance(
            std::ostream   &os);

    //----------------------------------------------------------------
    /**   インスタンスの後始末をする。
    **
    **  @return     エラーコードを返す。
    **      -   異常終了時はエラーの種類を示す非ゼロ値を返す。
    **      -   正常終了の場合はゼロを返す。
    **/
    const   ErrCode
    clearInstance();

//========================================================================
//
//    プログラムの読み込みとカーネルの準備。
//
public:

    //----------------------------------------------------------------
    /**   ソースファイルを指定する。
    **
    **  @param [in] filename    ファイル名。
    **  @return     エラーコードを返す。
    **      -   異常終了時はエラーの種類を示す非ゼロ値を返す。
    **      -   正常終了の場合はゼロを返す。
    **/
    const   ErrCode
    readSourceFile(
            const  std::string  &filename);

    //----------------------------------------------------------------
    /**   読み込んだソースコードからプログラムをビルドする。
    **
    **  @param[out] os    ログ出力用ストリーム。
    **  @return     エラーコードを返す。
    **      -   異常終了時はエラーの種類を示す非ゼロ値を返す。
    **      -   正常終了の場合はゼロを返す。
    **/
    const   ErrCode
    buildPrograms(
            std::ostream   &os);

    //----------------------------------------------------------------
    /**   ビルドしたプログラムからカーネルを作成する。
    **
    **  @param [in] kname   カーネル名。
    **  @return     エラーコードを返す。
    **      -   異常終了時はエラーの種類を示す非ゼロ値を返す。
    **      -   正常終了の場合はゼロを返す。
    **/
    const   ErrCode
    createKernel(
            const  std::string  &kname);

//========================================================================
//
//    メモリ管理。
//
public:

    //----------------------------------------------------------------
    /**   バッファを作成する。
    **
    **/
    cl::Buffer
    createBuffer(
        cl_mem_flags    flags,
        ::size_t        size,
        void  *         ptrHost = NULL,
        cl_int  *       ptrErr  = NULL)
    {
        return ( cl::Buffer(this->m_context, flags, size, ptrHost, ptrErr) );
    }

//========================================================================
//
//    カーネルの実行。
//
public:

    //----------------------------------------------------------------
    /**   コマンドキューを作成する。
    **
    **/
    cl::CommandQueue
    createCommandQueue(
        cl_command_queue_properties properties = 0,
        cl_int  *   ptrErr  = NULL)
    {
        return ( cl::CommandQueue(this->m_context, this->m_device, properties, ptrErr) );
    }
    
//========================================================================
//
//    Accessors.
//
public:

    //----------------------------------------------------------------
    /**   コンテキストを取得する。
    **
    **  @return     コンテキストの参照を返す。
    **/
    const  cl::Context  &
    getContext()  const
    {
        return ( (this->m_context) );
    }

    const   cl::Device  &
    getDevice()  const
    {
        return ( this->m_device );
    }

    //----------------------------------------------------------------
    /**   ビルドしたプログラムを取得する。
    **
    **  @return     プログラムの参照を返す。
    **/
    const  cl::Program  &
    getProgram()  const
    {
        return ( this->m_program );
    }

//========================================================================
//
//    Member Variables.
//
private:
    cl::Platform                m_clPlatform;
    cl::Context                 m_context;
    std::vector<cl::Device>     m_devices;
    cl::Device                  m_device;
    std::vector<std::string>    m_srcFiles;
    cl::Program                 m_program;
};

}   //  End of namespace  CLWRAP

#endif
