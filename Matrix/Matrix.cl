﻿
__kernel  void
mulMatrix1(
        __global  const  float  input1[],
        __global  const  float  input2[],
        __global  float         output[],
        const  int              matSize)
{
    int     col = get_global_id(0);
    int     row = get_global_id(1);
    float   tmp = 0.0;
    for ( int k = 0; k < matSize; ++ k ) {
        int  ik = row * matSize + k;
        int  kj = k * matSize + col;
        tmp += input1[ik] * input2[kj];
    }
    output[row * matSize + col] = tmp;
}


__kernel  void
mulMatrix2(
        __global  const  float  input1[],
        __global  const  float  input2[],
        __global  float         output[],
        const  int              matSize,
        __local   float         block1[],
        __local   float         block2[],
        const  int              blkSize)
{
    int  gx = get_global_id(0);
    int  gy = get_global_id(1);
    int  lx = get_local_id(0);
    int  ly = get_local_id(1);
    int  ofsLoc = (ly * blkSize) + lx;
    float   tmp = 0.0;

    for ( int i = 0; i < matSize; i += blkSize ) {
        block1[ofsLoc]  = input1[gy * matSize + (i + lx)];
        block2[ofsLoc]  = input2[(i + ly) * matSize + gx];
        barrier(CLK_LOCAL_MEM_FENCE);

        for ( int k = 0; k < blkSize; ++ k ) {
            tmp += (block1[ly * blkSize + k] * block2[k * blkSize + lx]);
        }
        barrier(0);
    }
    output[(gy * matSize) + gx] = tmp;
}
